PLEASE NOTE THE WIKI IS CURRENTLY BEING MIGRATED OVER FROM THE OLD MEDIAWIKI SYSTEM TO THE NEW SOURCEFORGE WIKI - SO PARTS MAY BE BROKEN....

KDIS Users Guide
==================

[TOC]

# What Is KDIS

KDIS is an open source implementation in C++ of [DIS (Distributed Interactive Simulation).](http://en.wikipedia.org/wiki/Distributed_Interactive_Simulation)

From version 2-7-0 KDIS is licensed under the FreeBSD licence, previous versions use the LGPL license. 

As of version **2-0-0** KDIS fully supports version 5(IEEE 1278.1), version 6(IEEE 1278.1a) and version 7(IEEE 1278.1-2012) which is currently in beta. 
KDIS is continuously supported and developed and current plans are to finish implementing the new version 7 changes as well as further documentation/tutorials. I am also looking to further expand in the future with better support for logging and generating simulation statistics. 

# [Change Log](http://kdis.sourceforge.net/KDIS_Change_Log.htm)

What changes have been made to the latest version of KDIS. 

# [Migration Guide](Migration_Guide)

Advice on migrating to the latest version of KDIS. 

# Setting Up Your Project

## [Using CMake To Create Your Project](Using_CMake_To_Create_Your_Project)

How to use CMake to set up KDIS projects automatically for your chosen IDE. 

## [Adding KDIS To A New/Existing Project Manually](Adding_KDIS_To_A_New-Existing_Project_Manually)

How to manually set up a new project or include KDIS into an existing project. 

# Class Documentation/Diagrams

## View The Latest

View the latest version class documentation [here](http://kdis.sourceforge.net/classdoc/)

## Download Archive

Download the latest version class documentation and old in compiled HTML Help (.chm) format[ here](Download_ClassDoc) 

# [Tests](Tests)

Getting started with the unit test framework. This section contains help on running the KDIS unit tests and how to add your own tests.

# [Tutorials](Tutorials)

Getting started with KDIS. This section contains tutorials to go with the examples that come with KDIS. Read more [here](Tutorials). 

# Support

Support can be found via e-mail or the [forum](http://sourceforge.net/forum/forum.php?forum_id=879756)
    
Karljj1 at Yahoo.com
Find me on [Twitter](http://twitter.com/KarlJamesJones)
Find me on [Linkedin ](http://uk.linkedin.com/pub/karl-jones/13/810/335)

# [Useful DIS Links](Useful_DIS_Links) 

Some useful links when learning about DIS and its uses. 

# Download

You can download the latest and previous versions of KDIS from **[here.](http://sourceforge.net/projects/kdis/files/)**